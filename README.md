# ECSL taller 

En este taller se hara una demo de como provisionar una VM con docker de forma stock, 
usando una imagen stock de centos(pero podria ser cualquier otra soportada fedora,centos,debian).


Ocupare algunos proyectos open source para provisionar, podria lograrse todo a mano
pero por facilidad y conveniencia usare las siguientes.

### TOOLS 

#### vagrant 
#### terraform 





# vagrant 

descargar e instalar 

https://www.vagrantup.com/downloads.html

## en el caso de fedora (sustituir )
rpm -Uvh vagrant.rpm


## link para buscar boxes (vm)
https://app.vagrantup.com/boxes/search


## ejemplo para ahora  ( Ubuntu 16.04 stock)

https://app.vagrantup.com/peru/boxes/ubuntu-16.04-server-amd64


## arrancar maquina

vagrant init peru/ubuntu-16.04-server-amd64 --box-version 20180703.01

vagrant up


# ansible


sudo pip install ansible

### sidenote if pip not installed
sudo easy_install pip


#ansible shell

 ansible vagrant -a "df" -i ../hosts 

## ansible galaxy

https://galaxy.ansible.com/


# Digitalocean  y Terraform

Para digitalocean se necesitara usar el token de API y el fingerprint de la llave ssh.

##agregar ssh key
https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/

## token API
https://www.digitalocean.com/community/tutorials/how-to-create-a-digitalocean-space-and-api-key

export DO_TOKEN , DO_FINGERPRINT
DO_TOKEN=MI_CLAVE \
DO_FINGERPRINT=Mi KEY
DO_TOKEN="key"
DO_FINGERPRINT="fingerprint"

ssh-keygen -E md5 -lf ~/.ssh/dokey.pub | awk '{print $2}'



##Terraform

Herramienta para provisionar maquinas virtuales usualmente en cloud env, pero puede ser usado
con muchos providers.



https://www.terraform.io/downloads.html

## install terraform 


cd /tmp 

wget https://releases.hashicorp.com/terraform/0.11.7/terraform_0.11.7_linux_amd64.zip

unzip terraform*

sudo cp terraform /usr/local/bin/

### usando terraform 



### inicializando el proyecto

terraform init

### probando el manifiesto
terraform plan

terraform plan \
-var "do_token=${DO_TOKEN}" \
-var "pub_key=$HOME/.ssh/dokey.pub" \
-var "pvt_key=$HOME/.ssh/dokey" \
-var "ssh_fingerprint=$DO_FINGERPRINT"



